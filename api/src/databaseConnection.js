const mysql = require('mysql');

const mysqlConnect = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'fast_shop'
});

mysqlConnect.connect(function (err) {
    if (err) {
        console.log(err);
        return;
    }
    console.log('Database connection OK');
});

module.exports = mysqlConnect;
