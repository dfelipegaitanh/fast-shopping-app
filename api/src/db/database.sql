drop schema IF EXISTS fast_shop;
create schema fast_shop;
use fast_shop;

create table categories
(
    id int auto_increment,
    name varchar(100) default '' not null,
    constraint categories_pk
        primary key (id)
);

create table products
(
    id int auto_increment,
    image varchar(200) default '' not null,
    name varchar(100) default '' not null,
    price float default 0 not null,
    description text null,
    category_id int not null,
    constraint products_pk
        primary key (id),
    constraint products_category_id_fk
        foreign key (category_id) references categories (id)
            on update cascade on delete cascade
);
