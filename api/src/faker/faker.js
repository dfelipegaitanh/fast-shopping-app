const faker = require('faker');
const mysqlConnection = require('../databaseConnection');
const numberOfCategories = 6;
const productsPerCategory = 10;

for (let i = 0; i < numberOfCategories; i++) {
    let company = faker.commerce.department();
    mysqlConnection.query('INSERT INTO categories (name) VALUES ("' + company + '")', (err, result) => {
        const jsonResult = JSON.parse(JSON.stringify(result));
        const categoryId = jsonResult.insertId;

        for (let j = 0; j < productsPerCategory; j++) {
            let product = {
                image: faker.image.image(),
                name: faker.commerce.productName(),
                price: faker.commerce.price(),
                description: faker.commerce.product() + ' ' + faker.commerce.productAdjective(),
                category_id: categoryId,
            };

            console.log(product);

            const productQuery =
                'INSERT INTO products (image,name,description,price,category_id) VALUES ("' +
                product.image +
                '" , "' +
                product.name +
                '", "' +
                product.description +
                '", ' +
                product.price +
                ', ' +
                product.category_id +
                ' )';

            mysqlConnection.query(productQuery, (err, result) => {
                if (err) {
                    console.log(err);
                }
            });
        }
    });
}

return;
