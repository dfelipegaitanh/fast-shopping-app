const express = require('express');
const router = express.Router();

const mysqlConnection = require('../databaseConnection');

router.get('/api/products', (req, res) => {
    mysqlConnection.query('select p.* , c.name as category from products p inner join categories c on p.category_id = c.id', (err, rows, fields) => {
        if (!err) {
            res.send(rows);
        } else {
            console.log(err);
        }
    });
});

router.get('/api/products/:id', (req, res) => {
    const productId = req.params.id;
    mysqlConnection.query('select p.* , c.name as category from products p inner join categories c on p.category_id = c.id where p.id = ' + productId, (err, rows, fields) => {
        if (!err) {
            res.send(rows[0]);
        } else {
            console.log(err);
        }
    });
});

module.exports = router;
