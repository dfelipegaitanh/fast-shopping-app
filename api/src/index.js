const express = require('express');
const app = new express();

app.set('port', 4000);
// app.use(express.json());
app.use(require('./routes/routes'));

app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});