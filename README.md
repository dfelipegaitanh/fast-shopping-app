# Fast Shopping App

## How to install
* Install [NodeJs](https://nodejs.org/es/download/package-manager/ "NodeJs")
* [Install the API](#how-to-install-the-api)
* [Install the FrontEnd](#how-to-install)
* Install [MySql](https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/ "MySql")/[MariaDb](https://mariadb.com/kb/en/getting-installing-and-upgrading-mariadb/ "MariaDb")
  * If you have problems with the installation, I recommend you to use [docker](https://docs.docker.com/engine/install/ "docker"). You can follow this [tuturial](https://platzi.com/tutoriales/1432-docker/3268-como-crear-un-contenedor-con-docker-mysql-y-persistir-la-informacion/ "tuturial") (***spanish***) to install your MySql Container
    * You may have some connection problems. [Here](#possible-errors- mysql-in-docker) is one of the ways to fix it.
* Import the database ``` mysql -u username -p database_name < api/src/db/database.sql ```
* Configure your database connection
* [Create fake data in your database](#create-fake-data)
* [Build the FrontEnd](#how-to-build-the-frontend)
* Build the NodeJs Server for the API

## Front

### How to Install the FrontEnd
```bash
cd front
npm install
```

### How to build the frontend

```bash
cd front
npm start
```

## API

### How to Install the Api

Within the root directory run this command 

```bash
npm install
```

### Configure your database connection

Open the file ***api/src/databaseConnection.js*** and modify the variable ***mysqlConnect***

```nodeJs
const mysqlConnect = mysql.createConnection({
    host: YOUR_HOST,
    user: USERNAME,
    password: PASSWORD,
    database: 'fast_shop'
});
```

The database was called ***fast_shop*** in the file  ***api/src/database.sql**. If you want to change the name, open that file and modify this

```sql
drop schema IF EXISTS fast_shop;
create schema fast_shop;
use fast_shop;
```

### Create Fake data

> This will create 6 categories and 10 products per categories

```bash
cd api
node src/faker/faker.js
```

### Possible errors Mysql in Docker

* If you got this error `sqlMessage: 'Client does not support authentication protocol requested by server; consider upgrading MySQL client'` run these comands in the MySql console

```sql
ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'password';
flush privileges;
```

------------

[Felipe Gaitán](https://www.linkedin.com/in/felipegaitan81/ "Felipe Gaitán")