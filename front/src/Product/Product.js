import React, { useState } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Hidden from '@material-ui/core/Hidden';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
    card: {
        display: 'flex',
        boxShadow: 'none',
        border: 'black',
    },
    cardDetails: {
        flex: 1,
    },
    cardMedia: {
        width: 160,
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 128,
        height: 128,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    }
}));

const Product = ({product, propsHistory}) => {

    const [qty] = useState(1);
    const classes = useStyles();

    const handleAddToCart = () => {
        propsHistory.push("/cart/" + product.id + "?qty=" + qty)
    }

    return (
        <Grid item md={6} xs={12}>
            <CardActionArea component="a" href="#" key={'product-' + product.id}>
                <Box border={1}>
                    <Card className={classes.card}>
                        <Hidden xsDown>
                            <CardMedia className={classes.cardMedia} image={product.image} />
                        </Hidden>
                        <div className={classes.cardDetails} key={product.id}>
                            <CardContent>
                                <Grid item xs container direction="column" spacing={2}>
                                    <Grid item xs>
                                        <Typography gutterBottom variant="subtitle1">
                                            {product.name}
                                        </Typography>
                                        <Typography variant="body2" gutterBottom>
                                            {product.category}
                                        </Typography>
                                        <Typography variant="body2" color="textSecondary">
                                            {product.description}
                                        </Typography>
                                    </Grid>
                                    <Grid item container spacing={3}>
                                        <Grid item md={6} xs={12}>
                                            <Button onClick={handleAddToCart} variant="outlined" color="primary">
                                                Add to cart
                                            </Button>
                                        </Grid>
                                        <Grid item md={6} xs={12}>
                                            <Typography variant="subtitle1" align={'right'}>
                                                $ {product.price}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </CardContent>
                        </div>
                    </Card>
                </Box>
            </CardActionArea>
        </Grid>
    );
    // <select value={qty} onChange={(e) => { setQty(e.target.value) }} ></select>
};

export default Product;
