import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";
import { useDispatch } from "react-redux";
import { addToCart, removeFromCart } from "../actions/cartActions";
import CardContent from "@material-ui/core/CardContent";
import CardActionArea from "@material-ui/core/CardActionArea";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import Hidden from "@material-ui/core/Hidden";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from '@material-ui/icons/Delete';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";


const useStyles = makeStyles((theme) => ({
    card: {
        display: 'flex',
        boxShadow: 'none',
        border: 'black',
    },
    cardDetails: {
        flex: 1,
    },
    cardMedia: {
        width: 160,
    },
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
    },
    image: {
        width: 80,
        height: 80,
    },
    img: {
        margin: 'auto',
        display: 'block',
        maxWidth: '100%',
        maxHeight: '100%',
    }
}));

const ProductCart = ({ product }) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const removeFromCartHandler = () => {
        dispatch(removeFromCart(product.id));
    };

    return (
        <Grid item md={12}>
            <CardContent>
                <CardActionArea component="a" href="#" key={'productCart-' + product.id}>
                    <Box border={1}>
                        <Card className={classes.card}>
                            <Hidden xsDown>
                                <CardMedia className={classes.cardMedia} image={product.image} />
                            </Hidden>
                            <div className={classes.cardDetails} key={product.id}>
                                <CardContent>
                                    <Grid container>
                                        <Grid item xs={6} spacing={2}>
                                            <Typography fontWeight="fontWeightBold">
                                                <Box fontWeight="fontWeightBold">
                                                    {product.name}
                                                    <IconButton aria-label="delete" onClick={removeFromCartHandler}>
                                                        <DeleteIcon />
                                                    </IconButton>
                                                </Box>
                                            </Typography>
                                            <Typography variant="body2" gutterBottom>
                                                {product.category}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={2} spacing={3}>
                                            <Typography variant="subtitle1" align={'right'}>
                                                Unit Value $ {product.price}
                                            </Typography>
                                        </Grid>
                                        <Grid spacing={3} item xs={1} />
                                        <Grid spacing={3} item xs={1} >
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <Typography variant="body2" gutterBottom>Qty</Typography>
                                                <Select native value={product.qty}
                                                        onChange={(e => {dispatch(addToCart(product.id, e.target.value))})}>
                                                    <option value={1}>1</option>
                                                    <option value={2}>2</option>
                                                    <option value={3}>3</option>
                                                    <option value={4}>4</option>
                                                    <option value={5}>5</option>
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item spacing={3} xs={2}>
                                            <Typography variant="subtitle1" align={'right'}>
                                                $ {product.price * product.qty}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </CardContent>
                            </div>
                        </Card>
                    </Box>
                </CardActionArea>
            </CardContent>
        </Grid>
    )
}

export default ProductCart;