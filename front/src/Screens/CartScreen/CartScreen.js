import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { addToCart } from "../../actions/cartActions";
import ProductCart from "../../ProductCart/ProductCart";
import { makeStyles } from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TotalToPay from "../../TotalToPay/TotalToPay";
import { Link } from "react-router-dom";
import lightBlue from "@material-ui/core/colors/lightBlue";
import CheckOutButton from "../../Buttons/CheckOutButton/CheckOutButton";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
}));

const CartScreen = (props) => {
    const classes = useStyles();

    const [marginBottom] = useState(4);
    const [marginTop] = useState(2);
    const [marginSides] = useState(5);
    const [padding] = useState(2);
    const [total, setTotal] = useState(0);
    const cart = useSelector(state => state.cart);
    const { cartItems } = cart;
    const [productId] = useState(props.match.params.productId);
    const qty = props.location.search ? Number(props.location.search.split('=')[1]) : 1;
    const dispatch = useDispatch();

    useEffect(() => {
        if (productId) {
            dispatch(addToCart(productId, qty));
            setTotal(cartItems.reduce((a, b) => a + (b.price * b.qty), 0));
        }
    }, []);

    const renderCartProducts = () => {
        return cartItems.map((product) => {
            return <ProductCart product={product} total={total} />;
        });
    }

    return (
        <div className={classes.root}>
            <Box borderBottom={0} color="text.primary" mt={marginTop} ml={marginSides}
                 mr={marginSides} pb={padding}>
                <Grid container md={12} spacing={3}>
                    {renderCartProducts()}
                </Grid>
            </Box>
            <Grid container md={12} spacing={1}>
                <Grid item md={8}>
                    <Box borderBottom={0} color="text.primary" ml={marginSides}
                         mr={marginSides} pb={padding}>
                        <Typography>
                            <Link to="/home" style={{ color: lightBlue['500'] }}>
                                Continue shopping
                            </Link>
                        </Typography>
                    </Box>
                </Grid>
                <Grid item md={4}>
                    <TotalToPay />
                </Grid>
            </Grid>
            <Grid container md={12} direction="row-reverse" spacing={1}>
                <Box borderBottom={0} color="text.primary" mt={0} ml={marginSides} mr={marginSides}>
                    <CheckOutButton />
                </Box>
            </Grid>
        </div>
    );
};

export default CartScreen;
