import React from 'react';
import Products from '../../Products/Products';
import SortBy from '../../SortBy/SortBy';

function HomeScreen(props) {
    const paginationPage = props.match.params.paginationPage;
    return (
        <div>
            <SortBy />
            <Products paginationPage={paginationPage} propsHistory={props.history} />
        </div>
    )
}

export default HomeScreen;
