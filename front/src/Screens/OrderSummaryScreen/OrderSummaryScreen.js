import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import CustomerInformation from "../../OrderSummary/CustomerInformation/CustomerInformation";
import OrderSummary from "../../OrderSummary/OrderSummary/OrderSummary";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    }
}));

const OrderSummaryScreen = () => {

    const classes = useStyles();

    const [marginBottom] = useState(4);
    const [marginTop] = useState(2);
    const [marginSides] = useState(5);
    const [padding] = useState(2);

    return (
        <div className={classes.root}>
            <Box borderBottom={0} color="text.primary" mt={marginTop} mb={marginBottom} ml={marginSides}
                 mr={marginSides} pb={padding}>
                <Grid container>
                    <Grid item md={6} sm={12}>
                        <CustomerInformation />
                    </Grid>
                    <Grid item md={6} sm={12}>
                        <OrderSummary />
                    </Grid>
                </Grid>
            </Box>
        </div>
    );
}

export default OrderSummaryScreen;