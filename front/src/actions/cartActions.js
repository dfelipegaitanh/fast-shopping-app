import Cookie from "js-cookie";
import { CART_ADD_ITEM, CART_DELETE_CART, CART_REMOVE_ITEM, CART_SAVE_SHIPPING } from "../constants/cartConstants";
import { getProduct } from '../utils/api';

const addToCart = (productId, qty) => async (dispatch, getState) => {
    try {
        const { data } = await getProduct(productId);
        dispatch({
            type: CART_ADD_ITEM,
            payload: {
                id: data.id,
                category: data.category,
                description: data.description,
                product: data.id,
                name: data.name,
                image: data.image,
                price: data.price,
                qty,
            },
        });
        const { cart: { cartItems } } = getState();
        Cookie.set('cartItems', JSON.stringify(cartItems));
    } catch (error) {
    }
}
const removeFromCart = (productId) => (dispatch, getState) => {
    dispatch({ type: CART_REMOVE_ITEM, payload: productId });
    const { cart: { cartItems } } = getState();
    Cookie.set('cartItems', JSON.stringify(cartItems));
}
const deleteCart = () => (dispatch, getState) => {
    dispatch({ type: CART_DELETE_CART });
    const { cart: { cartItems } } = getState();
    Cookie.set('cartItems', JSON.stringify(cartItems));
}
const saveShipping = (data) => (dispatch) => {
    dispatch({ type: CART_SAVE_SHIPPING, payload: data });
}

export { addToCart, removeFromCart , saveShipping, deleteCart }