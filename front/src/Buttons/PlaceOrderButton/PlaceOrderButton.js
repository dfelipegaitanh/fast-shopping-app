import React from "react";
import Button from "@material-ui/core/Button";

const PlaceOrderButton = () => {

    return (
        <Button variant="outlined" color="primary" href="/placeOrder">
            Place Order
        </Button>
    )

}

export default PlaceOrderButton;