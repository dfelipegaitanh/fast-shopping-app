import React from "react";
import Button from "@material-ui/core/Button";

const StartAgainButton = () => {

    return (
        <Button variant="outlined" color="primary" href="/home">
            Start Again
        </Button>
    )

}

export default StartAgainButton;