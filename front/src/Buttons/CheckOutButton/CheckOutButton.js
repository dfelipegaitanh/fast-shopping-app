import React from "react";
import Button from "@material-ui/core/Button";

const CheckOutButton = () => {

    return (
        <Button variant="outlined" color="primary" href="/orderSummary">
            Check Out
        </Button>
    )
}

export default CheckOutButton;