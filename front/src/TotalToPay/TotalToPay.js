import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import TotalToPaySummary from "../OrderSummary/TotalToPaySummary/TotalToPaySummary";

const TotalToPay = () => {

    const [marginBottom] = useState(4);
    const [marginTop] = useState(2);
    const [marginSides] = useState(5);
    const [padding] = useState(2);

    return (
        <Box borderBottom={0} color="text.primary" mt={marginTop} mb={marginBottom} ml={marginSides}
             mr={marginSides} pb={padding}>
            <Grid item md={12}>
                <Box title>
                    <Typography align={"right"} variant="h4" gutterBottom>
                        <TotalToPaySummary />
                    </Typography>
                </Box>
            </Grid>
        </Box>
    );

}

export default TotalToPay;