import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import FaceIcon from '@material-ui/icons/Face';
import Badge from '@material-ui/core/Badge';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import { Link } from "react-router-dom";
import grey from "@material-ui/core/colors/grey";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {},
    menuButton: {
        marginRight: theme.spacing(2),
    },
    grow: {
        flexGrow: 1,
    },
    testHeader: {
        boxShadow: 'none',
        borderBottom: 'beige',
    },
}));

const FastShoppingBar = () => {
    const classes = useStyles();
    const marginBottom = 1;
    const marginTop = 3;
    const marginSides = 5;
    const paddingBottom = 2;

    const cart = useSelector(state => state.cart);
    const { cartItems } = cart;

    return (
        <Box borderBottom={1} color="text.primary" mt={marginTop} ml={marginSides} mr={marginSides} mb={marginBottom}
             pb={paddingBottom} fontWeight="fontWeightBold" className={classes.root}>
            <AppBar position="static" color={'transparent'} className={classes.testHeader}>
                <Toolbar>
                    <Link to="/home">
                        <IconButton>
                            <FaceIcon style={{color: grey[900]}} />
                        </IconButton>
                    </Link>
                    <Typography variant="h6" className={classes.title}>
                        Fast Shopping
                    </Typography>
                    <div className={classes.grow} />
                    <div>
                        <Link to="/cart/">
                            <IconButton aria-label="shopping cart" color="inherit">
                                <Badge badgeContent={cartItems.length} color="secondary">
                                    <ShoppingCartIcon style={{color: grey[900]}} />
                                </Badge>
                            </IconButton>
                        </Link>
                    </div>
                </Toolbar>
            </AppBar>
        </Box>
    );
};
export default FastShoppingBar;
