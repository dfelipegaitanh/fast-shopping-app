import React from 'react';
import FastShoppingBar from './FastShoppingBar/FastShoppingBar';
import { BrowserRouter, Route } from 'react-router-dom';
import HomeScreen from './Screens/HomeScreen/HomeScreen';
import Box from '@material-ui/core/Box';
import CartScreen from "./Screens/CartScreen/CartScreen";
import OrderSummaryScreen from "./Screens/OrderSummaryScreen/OrderSummaryScreen";
import PlaceOrder from "./OrderSummary/PlaceOrder/PlaceOrder";

function App() {
    return (
        <Box pb={10}>
            <BrowserRouter>
                <div className="App">
                    <FastShoppingBar />
                    <Route path="/" exact={true} component={HomeScreen} />
                    <Route path="/home" exact={true} component={HomeScreen} />
                    <Route path="/orderSummary" exact={true} component={OrderSummaryScreen} />
                    <Route path="/pagination/:paginationPage" exact={true} component={HomeScreen} />
                    <Route path="/cart/:productId?" component={CartScreen} />
                    <Route path="/placeOrder/" component={PlaceOrder} />
                </div>
            </BrowserRouter>
        </Box>
    );
}

export default App;
