import React, { useState } from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import { useDispatch } from "react-redux";
import { saveShipping } from "../../actions/cartActions";

const CustomerInformation = () => {
    const [marginTop] = useState(2);
    const [padding] = useState(2);

    const [typeUser, setTypeUser] = useState('');
    const [fullName, setFullName] = useState('');
    const [id, setId] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [email, setEmail] = useState('');
    const dispatch = useDispatch();

    const submitHandler = () => {
        dispatch( saveShipping( {fullName, id, address, phoneNumber, email} ) );
    }

    const handleTypeUser = (event) => {
        if( event.target.value === "existingCustomer"){}
        setTypeUser(event.target.value);
        submitHandler()
    };

    const handleFullName = (event) => {
        setFullName(event.target.value);
        submitHandler()
    };
    const handleId = (event) => {
        setId(event.target.value);
        submitHandler()
    };

    const handleAddress = (event) => {
        setAddress(event.target.value);
        submitHandler()
    };

    const handlePhoneNumber = (event) => {
        setPhoneNumber(event.target.value);
        submitHandler()
    };

    const handleEmail = (event) => {
        setEmail(event.target.value);
        submitHandler()
    };

    return (
        <Box border={1} color="text.primary" mt={marginTop} p={padding}>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Grid container alignItems="flex-start" justify="flex-end" direction="row">
                        Are you?
                    </Grid>
                </Grid>
                <Grid item xs={8}>
                    <FormControl component="fieldset">
                        <RadioGroup row aria-label="position" name="position" defaultValue="top" onChange={handleTypeUser}>
                            <FormControlLabel value="newCustomer" control={<Radio color="primary" />} label="New Customer" labelPlacement="New Customer" />
                            <FormControlLabel value="existingCustomer" control={<Radio color="primary" />} label="Existing Customer" labelPlacement="Existing Customer" />
                        </RadioGroup>
                    </FormControl>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Grid container alignItems="flex-start" justify="flex-end" direction="row">
                        Full Name
                    </Grid>
                </Grid>
                <Grid item xs={8}>
                    <TextField fullWidth id="outlined-basic" label="Full Name" variant="outlined" onChange={handleFullName} />
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Grid container alignItems="flex-start" justify="flex-end" direction="row">
                        ID
                    </Grid>
                </Grid>
                <Grid item xs={8}>
                    <TextField fullWidth id="outlined-basic" label="Id" variant="outlined" onChange={handleId} />
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Grid container alignItems="flex-start" justify="flex-end" direction="row">
                        Address
                    </Grid>
                </Grid>
                <Grid item xs={8}>
                    <TextField id="filled-multiline-static" multiline fullWidth rows={4} variant="filled" onChange={handleAddress} />
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Grid container alignItems="flex-start" justify="flex-end" direction="row">
                        Phone Number
                    </Grid>
                </Grid>
                <Grid item xs={8}>
                    <TextField fullWidth id="outlined-basic" label="Phone Number" variant="outlined" onChange={handlePhoneNumber} />
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Grid container alignItems="flex-start" justify="flex-end" direction="row">
                        Email
                    </Grid>
                </Grid>
                <Grid item xs={8}>
                    <TextField fullWidth id="outlined-basic" label="Email" variant="outlined" onChange={handleEmail} />
                </Grid>
            </Grid>
        </Box>
    );
};

export default CustomerInformation;
