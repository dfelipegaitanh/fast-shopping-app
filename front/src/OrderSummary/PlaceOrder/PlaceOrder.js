import React, { useEffect, useState } from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import StartAgainButton from "../../Buttons/StartAgainButton/StartAgainButton";
import { Typography } from '@material-ui/core';
import { deleteCart } from "../../actions/cartActions";
import { useDispatch } from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    }
}));

const PlaceOrder = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [marginBottom] = useState(4);
    const [marginTop] = useState(2);
    const [marginSides] = useState(5);
    const [padding] = useState(2);

    useEffect(() => {
        dispatch(deleteCart());
        return () => {
        };
    }, []);

    return (
        <div className={classes.root}>
            <Box borderBottom={0} color="text.primary" mt={marginTop} mb={marginBottom} ml={marginSides}
                 mr={marginSides} pb={padding}>

                <div style={{
                    position: 'absolute',
                    left: '50%',
                    top: '50%',
                    transform: 'translate(-50%, -50%)'
                }}>
                    <Grid container className={classes.root} spacing={2} alignContent={"center"} alignItems={"center"}>

                        <Grid item xs={1} />
                        <Grid item xs={10} alignItems={"center"} alignContent={"center"}><Typography paragraph
                                                                                                     variant={"h5"}>For
                            your purchase</Typography></Grid>
                        <Grid item xs={1} /><Grid item xs={1} />
                        <Grid item xs={10}><Typography paragraph variant={"h5"}>Your Items will be soon at your
                            door</Typography></Grid>
                        <Grid item xs={1} /><Grid item xs={1} />
                        <Grid item xs={10}><Typography paragraph variant={"h5"}>Stay Safe</Typography></Grid>
                        <Grid item xs={1} /> <Grid item xs={1} />
                        <Grid item xs={10}><Typography paragraph><StartAgainButton /></Typography></Grid>
                    </Grid>
                </div>
            </Box>
        </div>
    );
}

export default PlaceOrder;