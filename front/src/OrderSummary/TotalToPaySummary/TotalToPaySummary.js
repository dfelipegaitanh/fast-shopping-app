import React from "react";
import { useSelector } from "react-redux";

const TotalToPaySummary = () => {

    const state = useSelector(state => state.cart);
    const { cartItems } = state;

    return <div>Total: $ {cartItems.reduce((a, b) => a + (b.price * b.qty), 0)}</div>

}

export default TotalToPaySummary;