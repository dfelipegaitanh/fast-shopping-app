import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import grey from "@material-ui/core/colors/grey";
import { useSelector } from "react-redux";
import Grid from "@material-ui/core/Grid";
import TotalToPaySummary from "../TotalToPaySummary/TotalToPaySummary";
import PlaceOrderButton from "../../Buttons/PlaceOrderButton/PlaceOrderButton";

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: grey[900],
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
    table: {
        minWidth: 700,
    },
}))(TableRow);

const useStyles = makeStyles({
    table: {
        minWidth: 700,
    },
});

const OrderSummary = () => {

    const classes = useStyles();
    const state = useSelector(state => state.cart);
    const { cartItems } = state;

    const [marginBottom] = useState(4);
    const [marginTop] = useState(2);
    const [marginSides] = useState(5);
    const [padding] = useState(2);

    return (
        <Box borderBottom={0} color="text.primary" mt={marginTop} mb={marginBottom} ml={marginSides}
             mr={marginSides} pb={padding}>
            <Grid md={12}>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="customized table">
                        <TableHead>
                            <TableRow>
                                <StyledTableCell>Product</StyledTableCell>
                                <StyledTableCell align="right">Unit Price</StyledTableCell>
                                <StyledTableCell align="right">Units</StyledTableCell>
                                <StyledTableCell align="right">Total Price</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {cartItems.map((row) => (
                                <StyledTableRow key={row.name}>
                                    <StyledTableCell component="th" scope="row"> {row.name} </StyledTableCell>
                                    <StyledTableCell align="right">$ {row.price}</StyledTableCell>
                                    <StyledTableCell align="right">{row.qty}</StyledTableCell>
                                    <StyledTableCell align="right">$ {row.price * row.qty}</StyledTableCell>
                                </StyledTableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
            <Grid md={12}>
                <Box borderBottom={0} color="text.primary" mt={2} mb={2}>
                    <Grid container direction="row" justify="flex-end">
                        <TotalToPaySummary />
                    </Grid>
                </Box>
            </Grid>
            <Grid md={12}>
                <Box borderBottom={0} color="text.primary" mt={2} mb={2}>
                    <Grid container direction="row" justify="flex-end">
                        <PlaceOrderButton />
                    </Grid>
                </Box>
            </Grid>
        </Box>
    );

}

export default OrderSummary;
