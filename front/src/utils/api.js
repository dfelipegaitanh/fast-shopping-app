import axios from "axios";

export function getProducts() {
    return axios.get('/api/products/');
}

export function getProduct(productId) {
    return axios.get('/api/products/' + productId);
}