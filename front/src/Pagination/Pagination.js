import React from 'react';
import { usePagination } from '@material-ui/lab/Pagination';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

const useStyles = makeStyles({
    ul: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
        display: 'flex',
    },
    button: {
        marginLeft: 10,
        marginRight: 10,
    },
});

export default function UsePagination({ pageCount }) {
    const classes = useStyles();
    const { items } = usePagination({
        count: pageCount,
    });

    return (
        <Grid container direction="row" justify="center" alignItems="center">
            <nav>
                <ul className={classes.ul}>
                    {items.map(({ page, type, selected, ...item }, index) => {
                        let children = null;

                        if (type === 'start-ellipsis' || type === 'end-ellipsis') {
                            children = <Typography className={classes.button}>…</Typography>;
                        } else if (type === 'page') {
                            children = (
                                <Link to={'/pagination/' + page}>
                                    <button type="button" style={{ fontWeight: selected ? 'bold' : undefined, cursor: 'pointer' }} {...item}>
                                        <Typography className={classes.button}>{page}</Typography>
                                    </button>
                                </Link>
                            );
                            /*} else {
                                children = (
                                    <button type="button" {...item} className={classes.button}>
                                        <Typography className={classes.button}>{type}</Typography>
                                    </button>
                                );*/
                        }

                        return <li key={index}>{children}</li>;
                    })}
                </ul>
            </nav>
        </Grid>
    );
}
