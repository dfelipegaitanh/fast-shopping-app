import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Box from "@material-ui/core/Box";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

const SortBy = () => {
    const classes = useStyles();
    const margin = 2;
    const padding = 2;

    return (
        <Box component="div" display="flex" alignItems="center" m={margin} p={padding} justifyContent="flex-end">
            <Typography>Sort By</Typography>
            <FormControl variant="outlined" className={classes.formControl}>
                <Select native inputProps={{name: 'age', id: 'filled-age-native-simple',}}>
                    <option aria-label="None" value="">None</option>
                    <option value={10}>Max Price</option>
                    <option value={20}>Min Price</option>
                </Select>
            </FormControl>
        </Box>
    );
};
export default SortBy;
