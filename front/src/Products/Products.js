import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Product from '../Product/Product';
import Pagination from '../Pagination/Pagination';
import { useDispatch, useSelector } from 'react-redux';
import { listProducts } from '../actions/productActions.';

// const useStyles = makeStyles((theme) => ({}));

const Products = (props) => {
    const [marginBottom] = useState(4);
    const [marginTop] = useState(2);
    const [marginSides] = useState(5);
    const [padding] = useState(2);
    const [perPage] = useState(20);
    const [pageCount, changePageCount] = useState(0);
    const productList = useSelector((state) => state.productList);
    const {products, loading, error} = productList;
    const dispatch = useDispatch();

    if (products !== undefined && products.length > 0 && pageCount === 0) changePageCount(Math.ceil(products.length / perPage));

    useEffect(() => {
        dispatch(listProducts());
        return () => {
        };
    }, []);

    const renderProducts = () => {
        let offset = (props.paginationPage === undefined ? 0 : props.paginationPage - 1) * perPage;

        const slice = products.slice(offset, offset + perPage);
        return slice.map((product) => {
            return <Product product={product} propsHistory={props.propsHistory}/>;
        });
    };

    return loading ? <div>Loading</div> : error ? <div>{error}</div> : (
        <div >
            <Box borderBottom={0} color="text.primary" mt={marginTop} mb={marginBottom} ml={marginSides}
                 mr={marginSides} pb={padding}>
                <Grid container spacing={3}>
                    {renderProducts()}
                </Grid>
            </Box>
            <Pagination pageCount={pageCount} />
        </div>
    );
};

export default Products;
