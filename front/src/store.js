import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import Cookie from 'js-cookie';
import { productListReducer } from './reducers/productReducers';
import { cartReducer } from "./reducers/cartReducer";

const cartItems = Cookie.getJSON("cartItems") || [];

const reducer = combineReducers({
    productList: productListReducer,
    cart: cartReducer,
});

const initialState = { cart: { cartItems, shipping: {}} };

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, initialState, composeEnhancer(applyMiddleware(thunk)));
export default store;